function cacheFunction(cb) {

    if (typeof cb !== 'function') {
        throw new Error('argument passed into function should be a call back function')
    }
    let cache = {}

    return function (...args) {
        let argsString = JSON.stringify(args)
        if (Object.keys(cache).includes(argsString)) {
            return cache[argsString]
        }
        else {
            let result=cb(...args)
            cache[argsString] = result
            return result
        }

    }
}
module.exports = cacheFunction