function limitFunctionCallCount(cb, n) {
    // try{
    if (typeof cb !== 'function') {
        throw new Error('first argument is not a call back function')
    }
    if (typeof n !== 'number') {
        throw new Error('second argument is not a number')
    }
    let count = 0

    return function (...args) {
        if (count < n) {
            count += 1
            return cb(...args)
        }
        return null

    }
    // }catch(error){
    //     console.log(error)
    // }

    // function (...args) {
    //     if (count < n) {
    //         count += 1
    //         cb(...args)
    //     }
}
module.exports = limitFunctionCallCount