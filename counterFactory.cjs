// let counter=1
function counterFactory() {
    let counter = 0
    // console.log(counter,'..........')
    // // let counter=0
    let obj = {
        increment() {
            // console.log(counter,'counter')
            counter += 1
            return counter
        },
        decrement() {
            counter -= 1
            return counter
        }
    }
    return obj
    // console.log(counter)
}


module.exports = counterFactory
