const limitFunctionCallCount = require('../limitFunctionCallCount.cjs')

try {
    let cb = (a, b) => {
        console.log(a, b)
    }
    let result = limitFunctionCallCount(cb, 2)
    result(1, 2)
    result(3, 5)
    result(6, 9)
    let a = result(1, 9)
    console.log(a)
}
catch (error) {
    console.log(error)
}